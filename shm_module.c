/*! Share memory module */

#include "shm_module.h"


//! create the share memory segment 
/*! 
	Creat a share memoey section as a file descriptor.  
	input: file name for the file descriptor. 
	input: shm_size for data
	output: int **shm_buf; start position of the share memory. 
	return -1 if error occured.
	return 1 if ok.
*/
int shm_create(unsigned char **shm_buf, const char *filename, const int shm_size){

	int fd;		//!< file descriptor 
	int filesize = shm_size+3; //!< data size + 3 flag bytes
	
	remove(filename);	//!< delete the segment if existing
	fd = open(filename, O_RDWR | O_CREAT, S_IRWXU); 	//!< create a file descriptor 
											//!<with read write and exercute premission 

	int result = lseek(fd, filesize*sizeof(unsigned char)-1, SEEK_SET); //!< move the file pointer to the end of file
	if (result == -1) {
		close(fd);
		perror("Error calling lseek() to 'stretch' the file");
		return -1;
	}
	write(fd, "", 1);
	
	*shm_buf = (unsigned char *)mmap(0, filesize*sizeof(unsigned char), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	//!< Map the file descriptor to share memory
	close(fd);
	return 1;

}

void shm_lock_init(unsigned char *shm_buf, const int shm_size){

	shm_buf[shm_size] = DONE; 		//!< initialize local read lock
	shm_buf[shm_size+1] = DONE; 	//!< initialize local write lock
	shm_buf[shm_size+2] = DONE;	//!< initialize global read/write lock

}


//! Link the share memory segment to file descriptor
/*! 
	Link a share memoey section as a file descriptor.  
	input: file name for the file descriptor. 
	output: int **shm_buf; start position of the share memory. 
	return -1 if error occured.
	return 1 if ok.
*/
int shm_link(unsigned char **shm_buf, const char *filename, const int shm_size){

	int fd;		//!< file descriptor 
	int filesize = shm_size+3;
	fd = open(filename, O_RDWR, S_IRWXU); 	//!< create a file descriptor 
											//!<with read write and exercute premission 
	
	if (fd < 0){
		printf("Error: file %s does not exist, please use shm_create(2) to create the file!\n", filename);
		return -1;
	}

	*shm_buf = (unsigned char *)mmap(0, filesize*sizeof(unsigned char), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	//!< Map the file descriptor to share memory
	close(fd);

	return 1;

}

//! read the content from the share memory and put it into rbuf
/*!
	input:
		*shm_buf: 	pointer to the share memory buffer
		shm_size: 	full size of share memory data
		start: 		startig index of shm, 0...inf
		length: 	length of memory segment to read
	
	output:
		*rbuf: 		pointer to recieving buffer
*/
void shm_read(unsigned char *rbuf, unsigned char *shm_buf, const int shm_size, const int start, const int length){

	unsigned char *read_lock_local;	//!< locak read lock 
	unsigned char *write_lock_local;	//!< locak write lock 
	
	read_lock_local = &shm_buf[shm_size];	//!< pointer to local read lock
	write_lock_local = &shm_buf[shm_size+1];	//!< pointer to local writ lock 
	//lock_global = &shm_buf[shm_size+2];		//!< pointer to gloal read/write lock

	while (*write_lock_local == BUSY){};	//!< no reading when shm is writing by other program

	*read_lock_local = BUSY;		//!< start reading, set read flag to busy
	//! copy the value in the buffer
	int kk = 0;
	int i;
	for (i=start; i<start+length; i++){	
		rbuf[kk] = shm_buf[i];
		kk++;
	}
	
	*read_lock_local = DONE; //!< finish reading, set read flag to done
}

//! write the content from the wbuf into shm_buf
/*!
	input:
		*wbuf: 	pointer to the share memory buffer
		shm_size: 	full size of share memory data
		start: 		startig index of shm, 0...inf
		length: 	length of memory segment to read

	output:
		*shm_buf: 	pointer to share memory
*/
void shm_write(unsigned char *shm_buf, unsigned char *wbuf, const int shm_size, const int start, const int length){

	unsigned char *read_lock_local;
	unsigned char *write_lock_local;
	read_lock_local = &shm_buf[shm_size];
	write_lock_local = &shm_buf[shm_size+1];

	while (*read_lock_local == BUSY){};

	*write_lock_local = BUSY;

	int kk = 0;
	int i;
	for (i=start; i<start+length; i++){
		shm_buf[i] = wbuf[kk];
		kk++;
	}

	*write_lock_local = DONE;

}

//! Remove the share memory 
void shm_destroy(unsigned char **shm_buf, const char *filename, const int shm_size){
	int filesize = shm_size+3;
	munmap(*shm_buf, filesize*sizeof(unsigned char));
	remove(filename);
}


/*********** Partial Data Struct ******/

void uchar2uint(unsigned int *ibuf, const unsigned char *cbuf, int bufno){
	union ui2uc x;
   int i;
	for (i=0; i<bufno/4; i++){
		x.c[0] = cbuf[4*i];
    	x.c[1] = cbuf[4*i+1];
    	x.c[2] = cbuf[4*i+2];
    	x.c[3] = cbuf[4*i+3]; 
    	ibuf[i] = x.ui;
  	}
}


void uint2uchar(unsigned char *cbuf, const unsigned int *ibuf, int bufno){
	union ui2uc x;
	int i;
	for (i=0; i<bufno/4; i++){
		x.ui = ibuf[i];
    	cbuf[4*i] = x.c[0];
    	cbuf[4*i+1] = x.c[1];
    	cbuf[4*i+2] = x.c[2];
    	cbuf[4*i+3] = x.c[3];
  	}
}

//!	combine 4 char to 1 float  
/*!
	input:
		cbuf: 	unsigned char array with size bufno
		bufno: 	no of unsigned char, must be multiple of 4
	output:
		fbuf: 	float array with size bufno/4
*/
void uchar2float(float *fbuf, const unsigned char *cbuf, int bufno){
	union f2uc x;
	int i;
	for (i=0; i<bufno/4; i++){
		x.c[0] = cbuf[4*i];
    	x.c[1] = cbuf[4*i+1];
    	x.c[2] = cbuf[4*i+2];
    	x.c[3] = cbuf[4*i+3]; 
    	fbuf[i] = x.f;
  	}
}

//! seperate 1 float to 4 char
/*!
	input:
		fbuf: 	float array with size bufno/4
		bufno: 	no of unsigned char, must be multiple of 4
	output:
		cbuf: 	unsigned char array with size bufno
*/
void float2uchar(unsigned char *cbuf, float *fbuf, int bufno){
	union f2uc x;
	int i;
	for (i=0; i<bufno/4; i++){
		x.f = fbuf[i];
    	cbuf[4*i] = x.c[0];
    	cbuf[4*i+1] = x.c[1];
    	cbuf[4*i+2] = x.c[2];
    	cbuf[4*i+3] = x.c[3];
  	}
}
