#include <stdlib.h>
#include <stdio.h>
#include "shm_module.h"

int main ()
{

int shm_size = 20;	//!< share memory size same as in server
unsigned char *shm_buf;
const char *filename = "../shm.bin";

unsigned char Rxbuf[20];

int fstatus = shm_link(&shm_buf, filename, shm_size);	//!< connect to server
if (fstatus == -1) exit(1);

int k;
for ( k=0; k < 20; k++)	{

	//! wait until hand shaking flag allow to read
	while (shm_buf[shm_size+2] != 127){}; //!< hand shaking flag 

	shm_read(Rxbuf, shm_buf, shm_size, 0, 10); //!< read the buffer

	shm_buf[shm_size+2] = 126; 	//!< hand shaking flag 

	//! print out the recieved buffer
	printf("printing buf at %d.\n", k);
	int i;
	for ( i = 0; i<10; i++){
		printf("%d ", Rxbuf[i]);
	}
	printf("\n");
	
}

return 0;
}
