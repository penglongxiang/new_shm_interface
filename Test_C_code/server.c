#include <stdlib.h>
#include <stdio.h>
#include "shm_module.h"

int main ()
{

int shm_size = 20;
unsigned char *shm_buf;
const char *filename = "../shm.bin";

int fstatus = shm_create(&shm_buf, filename, shm_size);

if (fstatus == -1) exit(1);
shm_lock_init(shm_buf, shm_size);
/* Prepare a file large enough to hold an unsigned integer. */

unsigned char Txbuf[20];

shm_buf[shm_size+2] = 126; //!< hand shaking flag for write 
printf("Writing 10 Bytes of Data...\n");
int k;
for ( k=0; k < 20; k++){

	printf("writing buffer at %d.\n", k);
	int i;
	for ( i=0; i<10; i++){
		//sprintf((unsigned char*) shm_buf+4*i, "%d", i);
		Txbuf[i] = k;
		printf("%d ", Txbuf[i] );
	}
	printf("\n");

	//! wait until hand shaking flkag allow to write
	while (shm_buf[shm_size+2] == 127){}; //!< hand shaking flag, 
	shm_write(shm_buf, Txbuf, shm_size, 0, 10);
	shm_buf[shm_size+2] = 127;	//!< hand shaking flag for read

}


/* Release the memory (unnecessary because the program exits). */
shm_destroy(&shm_buf, filename, shm_size);
return 0;
}

