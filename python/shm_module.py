# shm_module contain the following fucntion

# define BUSY 126	//!< vaule represent busy for read/write flags
# define DONE 127	//!< vaule represent free for read/write flags

# //!  create the share memory segment
# int shm_create(unsigned char **shm_buf, const char *filename, const int shm_size);

# //! initialize the lock in the share memory
# void shm_lock_init(unsigned char *shm_buf, const int shm_size);

# //! Link the share memory segment to file descriptor
# int shm_link(unsigned char **shm_buf, const char *filename, const int shm_size);

# //! read the content from the share memory and put it into rbuf
# void shm_read(unsigned char *rbuf, unsigned char *shm_buf, const int shm_size, const int start, const int length);

# //! write the content from the wbuf into shm_buf
# void shm_write(unsigned char *shm_buf, unsigned char *wbuf, const int shm_size, const int start, const int length);

# //! Remove the share memory
# void shm_destroy(unsigned char **shm_buf, const char *filename, const int shm_size);

# now you can call the function in shm_moudle.c
# byref is pass byref in C

########################################################
from os import sys
# import the ctypes library
from ctypes import *
# import the C shm_module
libc = cdll.LoadLibrary("../libshm_module.so")
# define the input type of all function in library
#libc.shm_create.argtypes=[POINTER(POINTER(c_ubyte)), c_char_p, c_int]
#libc.shm_lock_init.argtypes=[POINTER(c_ubyte), c_int]
#libc.shm_link.argtypes=[POINTER(POINTER(c_ubyte)), c_char_p, c_int]
#libc.shm_read.argtypes=[POINTER(c_ubyte), POINTER(c_ubyte), c_int, c_int, c_int]
#libc.shm_write.argtypes=[POINTER(c_ubyte), POINTER(c_ubyte), c_int, c_int, c_int]
#libc.shm_destroy.argtypes=[POINTER(POINTER(c_ubyte)), c_char_p, c_int]

# create a share memory segment
# input:    filename = file descriptor for the share memory
#           shm_size = memory buffer size
# output:   shm_buf = pointer for the share memory
def shm_create(filename, shm_size):
    # use the ctypes' variable type
    # c_ubyte = unsigned char in C, create a empty pointer for shm_buf
    shm_buf = POINTER(c_ubyte)()
    flag = libc.shm_create(byref(shm_buf), filename, shm_size)
    assert (flag == 1)  #if flag != 1, failed to create
    return shm_buf


# initialize the share memory lock : 126 = BUSY, 127 = DONE
# input:    shm_buf = the memory buffer
#           shm_size = memory buffer size
def shm_lock_init(shm_buf, shm_size):
    # initialize the lock ing the share memory
    libc.shm_lock_init(shm_buf, shm_size)
    return

# Link the share memory segment to file descriptor
# input:    filename = file descriptor for the share memory
# output:   shm_buf = pointer for the share memory
def shm_link(filename, shm_size):
    shm_buf = POINTER(c_ubyte)()    # define the pointer to share memory
    flag = libc.shm_link(byref(shm_buf), filename, shm_size)
    if (flag == 1):
        return shm_buf
    else:
        return None

# read a segment of the share memory
# input:    shm_buf = the memory buffer
#           shm_size = memory buffer size
#           start = starting position of the segment
#           length = the length of the segment
# output:   rbuf = an array contain the segment
def shm_read(shm_buf, shm_size, start, length):
    assert ((length+start) <= shm_size) #   check if the start position and length is valid
    rbuf = (c_ubyte*length)()
    libc.shm_read(byref(rbuf), shm_buf, shm_size, start, length)
    return rbuf


# write date to an segment of the share memory
# input:    shm_buf = the memory buffer going to write
#           wbuf_list = array or list contain the data to be written
#           shm_size = memory buffer size
#           start = starting position of the segment
#           length = the length of the segment
def shm_write(shm_buf, wbuf_list, shm_size, start, length):
    assert(start+length <= shm_size)
    wbuf_uchar = (c_ubyte*length)()
    if (length == 1):
        wbuf_uchar[0] = wbuf_list
    else:
        for i in range(0, length):
            wbuf_uchar[i]=wbuf_list[i]

    libc.shm_write(shm_buf, wbuf_uchar, shm_size, start, length)
    return

# destroy the share memory
# input:    shm_buf = pointer to the memory buffer
#           filename = file descriptor for the share memory
#           shm_size = memory buffer size
def shm_destroy(shm_buf, filename, shm_size):
    libc.shm_destroy(byref(shm_buf), filename, shm_size)
    return

# LOCK global lock for the share memory, can act as hand shaking signal
# this set the global lock to BUSY
# input:    shm_buf = pointer to the memory buffer
#           shm_size = memory buffer size
def shm_G_lock(shm_buf, shm_size):
    shm_buf[shm_size+2]=126

# UNLOCK global lock for the share memory, can act as hand shaking signal
# this set the global lock to FREE/DONE
# input:    shm_buf = pointer to the memory buffer
#           shm_size = memory buffer size
def shm_G_unlock(shm_buf, shm_size):
    shm_buf[shm_size+2]=127

# Check global lock status for the share memory,
# return 1 for BUSY, return 0 for FREE/DONE
# input:    shm_buf = pointer to the memory buffer
#           shm_size = memory buffer size
def shm_check_G_lock(shm_buf, shm_size):
    if shm_buf[shm_size+2] == 126:
        return 1
    if shm_buf[shm_size+2] == 127:
        return 0



## data type conversion
def uchar2float(buf_any, ucbufno):
    ucbuf = (c_ubyte*ucbufno)()
    for i in range(0, ucbufno):
        ucbuf[i]=buf_any[i]
    fbufno = ucbufno/4
    fbuf = (c_float*int(fbufno))()
    libc.uchar2float(fbuf, ucbuf, ucbufno)
    return fbuf

def float2uchar(buf_any, ucbufno):
    ucbuf = (c_ubyte*ucbufno)()
    fbufno = ucbufno/4
    fbuf = (c_float*int(fbufno))()
    for i in range(0, fbufno):
        fbuf[i]= buf_any[i]
    libc.float2uchar(ucbuf, fbuf, ucbufno)
    return ucbuf

def uchar2uint(buf_any, ucbufno):
    ucbuf = (c_ubyte*ucbufno)()
    for i in range(0, ucbufno):
        ucbuf[i]=buf_any[i]
	ibufno = ucbufno/4
    ibuf = (c_int32*int(ibufno))()
    libc.uchar2uint(ibuf, ucbuf, ucbufno)
    return ibuf

def uint2uchar(buf_any, ucbufno):
    ucbuf = (c_ubyte*ucbufno)()
    ibufno = ucbufno/4
    ibuf = (c_int32*int(ibufno))()
    for i in range(0, ibufno):
    	ibuf[i]= buf_any[i]
    libc.uint2uchar(ucbuf, ibuf, ucbufno)
    return ucbuf



