# import library
from com_interface import *
from shm_module import *
import time 

#test Add by susan, 20150527
mytime = 1;
def sleep(mytime):
	time.sleep(mytime)

# You need to care about the Example test code section only and you can ignore the server code
filename = '../shm_VS.bin'


# server code, not use in visual servoing
shm_size =70
shm_buf_s = shm_create(filename, shm_size)
shm_lock_init(shm_buf_s, shm_size)
flag_to_robot = 1
flag_from_robot = 0
shm_write(shm_buf_s, flag_to_robot, shm_size, 67,1)
shm_write(shm_buf_s, flag_from_robot, shm_size, 68,1)


# connect to share memory
shm_mem = link_port(filename)

# icount = 1
# uccount = uint2uchar(icount)
# print 'uccount = ', uccount[0:4]
# shm_write(shm_mem, uccount, shm_size, 0, 4)

#Tx = [3.15, 4.2, 9.8, 552.1]
#ucTx = float2uchar(Tx, 16)
#shm_write(shm_buf_s, ucTx, shm_size, 23, 16)
#print 'Tx = ', Tx

## Example test code, will use in vs code


if (shm_mem == None):
	print "no share memory segment, please check the share memory filename"
else:

	# read current pos from robot
	curpos = read_from_robot(shm_mem)
	print 'curpos = ', curpos[0:4]

	# send cmd and next pos to robot
	cmd = 2 	#range form 0-255
	speed = [100, 50, 50]
	send_to_robot(shm_mem, cmd, speed)

	curpos = read_from_robot(shm_mem)
	print 'nexpos = ', curpos[0:4]
	print 'cmd = ',cmd
	#test Add by susan, 20150527
	sleep(2)
	# send cmd and next pos to robot
	cmd = 1 	#range form 0-255
	nextpos = [280, 50, 0, 0]
	send_to_robot(shm_mem, cmd, nextpos)

	curpos = read_from_robot(shm_mem)
	print 'nexpos = ', curpos[0:4]
	print 'cmd = ',cmd

	# reas moving status
	mvflag = get_move_flag(shm_mem)
	print 'mvflag = ', mvflag




###### back to server #######
	# uccount = shm_read(shm_buf_s, shm_size, 0, 4)
	# icount = uchar2uint(uccount)
	# print 'icount = ', icount

	# cmd = shm_read(shm_buf_s, shm_size, 4, 1)
	# print 'cmd = ', cmd[0]

	# Rx = shm_read(shm_buf_s, shm_size, 5, 16)
	# fRx = uchar2float(Rx,16)
	# print 'Rx = ' , fRx[0:4]

	# read the byte 67
	status = shm_read(shm_buf_s, shm_size, 67, 1)
	print 'status = ', status[0]

	# shm_destroy(shm_buf_s, filename, shm_size)
