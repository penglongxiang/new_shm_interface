from shm_module import *
# IMPORTANT: You can only enable either server or client
mode = "server"
#mode = "clienr"

if mode == "client":

    ##################### BELOW is a CLIENT #################################
    # comment or uncomment tis part fro disable or enable the client
    # Pyhton share memory test client

    # assigned share memory size
    shm_size = 20
    # assigned name for the file descriptor
    filename = "../shm.bin"
    # link the client to the share memory created by server
    mem = shm_link(filename, shm_size)

    # read the memory for 20 times
    for k in range(0,20):
        # check if the server is writing the memory
        while shm_check_G_lock(mem, shm_size) == 1:
            pass
        # read the first 10 data of the memory
        Rx = shm_read(mem, shm_size, 0, 10)
        # tell the server reading is finished
        shm_G_lock(mem, shm_size)
        # print out the data
        print Rx[0:9]

if mode == "server":
    ##################### BELOW is a SERVER #################################
    #  comment or uncomment tis part fro disable or enable the server
    # # pyhton share memory test server

    # assigned share memory size
    shm_size = 20
    # assigned name for the file descriptor
    filename = "../shm.bin"
    # create the share memory
    mem = shm_create(filename, shm_size)
    # initialize the lock
    shm_lock_init(mem, shm_size)

    print "writing 10 bytes of data...\n"
    # treat the global lock as hand shaking signal
    shm_G_lock(mem, shm_size)   # lock the Global Lock to tell the client server is writing
    # write the memory for 20 times
    for k in range(0,20):
        print "writing buffer at ",k,"\n"
        Tx = [k]*10 # Data to write
        print Tx

        # check if the client is reading the memory
        while shm_check_G_lock(mem, shm_size) == 0:
            pass

        # write the memory
        shm_write(mem, Tx, shm_size, 0, 10)
        # unlock the global lock to give the reading right to the client
        shm_G_unlock(mem, shm_size)

    # delete the memory when finish
    shm_destroy(mem, filename, shm_size)