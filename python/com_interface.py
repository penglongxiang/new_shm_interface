# import the share memory library
from shm_module import *

# set the memory size, always 40 bytes, defined bu Jason 
shm_size = 70

# Link the share memory segment, return None if the file does not exist
def link_port(filename):
	shm_buf = shm_link(filename, shm_size)
	return shm_buf

# command counter adder
def add_count(shm_buf):
	shm_count = shm_read(shm_buf, shm_size, 0, 4)
	count_i = uchar2uint(shm_count,4)
	count_i[0] = count_i[0] + 1
	ucbuf = uint2uchar(count_i,4)
	shm_write(shm_buf, ucbuf, shm_size, 0, 4)
	return

# get the current robot pos, 
# return is a c_float array, the value must read with []
# i.e. fpos[0:4]  
def read_from_robot(shm_buf):
	ucpos = shm_read(shm_buf, shm_size, 37, 16)
	fpos = uchar2float(ucpos, 16)
	return fpos

# send next pos to robot, 
# modified by jason 20150527
def send_to_robot(shm_buf, cmd, fpos):
	if cmd == 1 :
		ucpos = float2uchar(fpos,16)
		shm_write(shm_buf, cmd, shm_size, 4, 1)
		shm_write(shm_buf, ucpos, shm_size, 5, 16)
		add_count(shm_buf)
	elif cmd == 2:
		ucspeed = uint2uchar(fpos,12)
		shm_write(shm_buf, cmd, shm_size, 4, 1)
		shm_write(shm_buf, ucspeed, shm_size, 5, 12)
		add_count(shm_buf)
	return

def get_move_flag(shm_buf):
	flag = shm_read(shm_buf, shm_size, 68,1)
	flag_to_robot = 0 
	shm_write(shm_buf, flag_to_robot, shm_size, 67, 1)
	return flag[0]


