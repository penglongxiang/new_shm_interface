#INSTALLATION

1. Just type "make" in this terminal, a libshm_module.so will be generated in the current folder.

2. The python/shm_module.py is the wrapper for libshm_module.so, the use of shm_module is similar to shm_module in C code. Please check the detail API in the shm_module.py

3. The python/com_interface.py is the communication module between Jason's robot and Malin's visual servoing

4. The python/com_interface_test.py is an example to illustrate how to use the the com_interface module

6. Using the shm_moudle.py and com_interface.py in other place:
	You just need to copy the libshm_module.so to any folder, then change the library path in the shm_module.py line 32, then you can use it easily  