#ifndef __SHM_MODULE_H__
#define __SHM_MODULE_H__

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define BUSY 126	//!< vaule represent busy for read/write flags
#define DONE 127	//!< vaule represent free for read/write flags
//! create the share memory segment 
/*! 
	Creat a share memoey section as a file descriptor.  
	input: file name for the file descriptor. 
	output: int **shm_buf; start position of the share memory. 
	return -1 if error occured.
	return 1 if ok.
*/

int shm_create(unsigned char **shm_buf, const char *filename, const int shm_size);

//! Initialize the share memory lock
/*! 
	input: *shm_buf: 	pointer to the share memory buffer
	shm_size: 	full size of share memory data
*/
void shm_lock_init(unsigned char *shm_buf, const int shm_size);

//! Link the share memory segment to file descriptor
/*! 
	Link a share memoey section as a file descriptor.  
	input: file name for the file descriptor. 
	output: int **shm_buf; start position of the share memory. 
	return -1 if error occured.
	return 1 if ok.
*/

int shm_link(unsigned char **shm_buf, const char *filename, const int shm_size);
//! read the content from the share memory and put it into rbuf
/*!
	input:
		*shm_buf: 	pointer to the share memory buffer
		shm_size: 	full size of share memory data
		start: 		startig index, 0...inf
		length: 	length of memory segment to read
	
	output:
		*rbuf: 		pointer to recieving buffer
*/
void shm_read(unsigned char *rbuf, unsigned char *shm_buf, const int shm_size, const int start, const int length);
//! write the content from the wbuf into shm_buf
/*!
	input:
		*wbuf: 	pointer to the share memory buffer
		shm_size: 	full size of share memory data
		start: 		startig index, 0...inf
		length: 	length of memory segment to read

	output:
		*shm_buf: 	pointer to share memory
*/
void shm_write(unsigned char *shm_buf, unsigned char *wbuf, const int shm_size, const int start, const int length);


//! Remove the share memory 
void shm_destroy(unsigned char **shm_buf, const char *filename, const int shm_size);


//! define a union for 4 8-bit unsigned char and 32-bit unsigned int
union ui2uc{
	unsigned int ui;
	unsigned char c[4];
};
//! define a union for 2 8-bit unsigned char and 16-bit unsigned short
union char2short{
	unsigned short sint;
	unsigned char c[2];
};

//! define a union for 4 8-bit unsigned char and 32-bit float
union f2uc{
	float f;
	unsigned char c[4]; 
};


void uchar2uint(unsigned int *buf, const unsigned char *cbuf, int bufno);

void uint2uchar(unsigned char *cbuf, const unsigned int *buf, int bufno);
//!	combine 4 char to 1 float  
/*!
	input:
		cbuf: 	unsigned char array with size bufno
		bufno: 	no of unsigned char, must be multiple of 4
	output:
		fbuf: 	float array with size bufno/4
*/
void uchar2float(float *fbuf, const unsigned char *cbuf,  int bufno);
//! seperate 1 float to 4 char
/*!
	input:
		fbuf: 	float array with size bufno/4
		bufno: 	no of unsigned char, must be multiple of 4
	output:
		cbuf: 	unsigned char array with size bufno
*/
void float2uchar(unsigned char *cbuf, float *fbuf, int bufno);


#endif

