#gcc -c -Wall -Werror -fpic shm_module.c
#gcc -shared -o libshm_module.so shm_module.o
APP = libshm_module.so
# load source 
SRC = shm_module.c
#OBJ = $(SRC:.c=.o)
OBJ = shm_module.o

# set compiling flags
CC = gcc
CFLAGS = -Wall -Werror -fpic
CLIBTYPE = -shared

all: $(APP)

$(APP) : $(OBJ) 
	$(CC) $(CLIBTYPE) -o $(APP) $(OBJ) 

$(OBJ) : $(SRC)
	$(CC)  $(CFLAGS) -c -o $@ $< 

clean:
	rm -f *.o ; rm $(APP)
